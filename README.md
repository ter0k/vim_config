# vim_config

=======[ ter0k .vimrc ]=======

Plugin List (vimawesome.com):

  - indentline            : set '¦' to clearly see indentation
  - youcompleteme              : autocomplete python function and variable but I style use ^x^f to autocomplete directory path
  - auto-pairs            : auto close [, (, ', ...
  - ctrlp                 : Full path fuzzy
  - molokai               : syntax color
  - nerdtree              : file arborescence
  - nerdtree-git-plugin   : show modify files
  - syntastic             : messages for syntastic error
  - tagbar                : show variables, class, method, etc
  - vim-listtrans         : just make a list with sentance separate with ';' character
  - fugitive              : module git
  - gitv                  : fugitive extention. Show git tree
  - undotree              : show modification
  - csv.vim               : better view for csv

Installation
============
```bash
$ git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```
… then run the following in Vim:
```vim
:source %
:PluginInstall
```
and then :
```bash
$ cd ~/.vim/bundle/youcompleteme/
$ ./install.py
```
To use https://github.com/majutsushi/tagbar you need ctags