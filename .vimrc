" =======[ ter0k .vimrc ]=======
" My list of plugin:
"   - indentline            : set '¦' to clearly see indentation
"   - youcompleteme              : autocomplete python function and variable but I style use ^x^f to autocomplete directory path
"   - auto-pairs            : auto close [, (, ', ...
"   - ctrlp                 : Full path fuzzy
"   - molokai               : syntax color
"   - nerdtree              : file arborescence
"   - nerdtree-git-plugin   : show modify files
"   - syntastic             : messages for syntastic error
"   - tagbar                : show variables, class, method, etc
"   - vim-listtrans         : just make a list with sentance separate with ';' character
"   - fugitive              : module git
"   - gitv                  : fugitive extention. Show git tree
"   - undotree              : show modification
"   - csv.vim               : better view for csv

set nocompatible              " be iMproved, required
filetype off                  " required
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'xuyuanp/nerdtree-git-plugin'
Plugin 'tpope/vim-fugitive'
Plugin 'gregsexton/gitv'
Plugin 'scrooloose/syntastic'
Plugin 'kien/ctrlp.vim'
Plugin 'valloric/youcompleteme'
Plugin 'fatih/molokai'
Plugin 'yggdroot/indentline'
Plugin 'chun-yang/auto-pairs'
Plugin 'majutsushi/tagbar'
Plugin 'chrisbra/csv.vim'


call vundle#end()
filetype plugin indent on

"------CUSTOM------"
colorscheme molokai
syntax on
set relativenumber
set number
set tabstop=4
set expandtab
set autoindent
set smartindent
set shiftwidth=4
set ignorecase
set showcmd
set wildmenu
set incsearch
set cursorline
set hlsearch
set laststatus=2
set encoding=utf-8

packadd termdebug

function! DefaultColor(...)
    hi PmenuSel guibg=DarkGrey ctermfg=15 guifg=White ctermbg=0
endfunction

let g:changeId = b:changedtick

function! Leave(...)
    if g:changeId != b:changedtick
        let g:changeId = b:changedtick
        hi PmenuSel guibg=Red ctermfg=1* guifg=Black ctermbg=0
    else
        call DefaultColor()
    endif
endfunction

function! InsertPmenuSelColor(mode)
    if a:mode == 'i'
        hi PmenuSel guibg=Cyan ctermfg=6 guifg=Black ctermbg=0
    elseif a:mode == 'r'
        hi PmenuSel guibg=Purple ctermfg=5 guifg=Black ctermbg=0
    else
        hi PmenuSel guibg=DarkGrey ctermfg=1 guifg=Black ctermbg=0
    endif
endfunction

function! Saved()
    hi PmenuSel guibg=Green ctermfg=2 guifg=Black ctermbg=0
    let tempTimer = timer_start(1000, 'DefaultColor')
endfunction

au InsertEnter * call InsertPmenuSelColor(v:insertmode)
au InsertLeave * call Leave()
au BufWritePre * call Saved()
au TextChangedI * hi PmenuSel guibg=Red ctermfg=1* guifg=Black ctermbg=0

function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

" StatusLine
set statusline=
set statusline+=%#PmenuSel#
set statusline+=%f
set statusline+=%#LineNr#
set statusline+=%<\ \ %P
set statusline+=\ \ [%{tagbar#currenttag('%s','')}]
set statusline+=%=
set statusline+=%#Pmenu#
set statusline+=%{StatuslineGit()}
set statusline+=%#LineNr#


" Highligh the 75st char of a line in red. For exemple the red char is here
highlight ColorColumn ctermbg=red
call matchadd('ColorColumn', '\%75v')

" set a dot for all whitespace at the end of a line like this
exec "set listchars=trail:\uB7"
set list

" open a term at the bottom
cabbrev term botright term

map <F2> :NERDTree <CR>
map <F3> :Tagbar <CR>
map <F4> :call ListTrans_toggle_format()<CR>
map <F5> :Gitv<CR>

map <C-Left> :tabprevious<CR>
map <C-Right> :tabnext<CR>

map <C-Up> :m .-2<CR>
map <C-Down> :m .+1<CR>

map ,, :s/^/#/<CR>
map ,. :s/^#//g <CR>

map gl :nohlsearch<CR>

" set a the very first line of your paragraph
nnoremap m1 :normal! kmmjdd{p`m<cr>

"------PLUGIN_PART------"
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_rust_checkers = ['rustc']

